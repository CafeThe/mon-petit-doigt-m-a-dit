import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  pictures = [
    {src: '../../assets/img/nails/arc-en-ciel.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/rouge-fonce.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/baby-violet.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/jaune.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/orange-neon.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/papillon-2.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/rouge.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/sailor-moon.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/vert.JPG', alt: 'rouge foncé'},
  ];

  breakpoint;

  constructor() { }

  ngOnInit(): void {
    this.calculateBreakpoint();
  }

  @HostListener('window:resize', ['$event'])
  onResize(){
    this.calculateBreakpoint();
  }

  private calculateBreakpoint() {
    if (window.innerWidth <= 600) {
      this.breakpoint = 1;
    } else if (window.innerWidth <= 1000) {
      this.breakpoint = 2;
    } else {
      this.breakpoint = 3;
    }
  }
}
