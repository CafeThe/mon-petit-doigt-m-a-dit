import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PricesComponent} from './prices/prices.component';
import {ContactComponent} from './contact/contact.component';
import {GalleryComponent} from './gallery/gallery.component';
import {ServicesComponent} from './services/services.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'accueil', component: HomeComponent},
  {path: 'prestations', component: ServicesComponent},
  {path: 'tarifs', component: PricesComponent},
  {path: 'galerie', component: GalleryComponent},
  {path: 'contact', component: ContactComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
