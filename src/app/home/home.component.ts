import {Component, HostListener, OnInit} from '@angular/core';

export interface Tile {
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  pictures = [
    {src: '../../assets/img/nails/arc-en-ciel.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/rouge-fonce.JPG', alt: 'rouge foncé'},
    {src: '../../assets/img/nails/baby-violet.JPG', alt: 'rouge foncé'},
  ];

  breakpoint: number;

  constructor() { }

  ngOnInit(): void {
    this.calculateBreakpoint();
  }

  @HostListener('window:resize', ['$event'])
  onResize(){
    this.calculateBreakpoint();
  }

  private calculateBreakpoint() {
    if (window.innerWidth <= 600) {
      this.breakpoint = 1;
    } else if (window.innerWidth <= 1000) {
      this.breakpoint = 2;
    } else {
      this.breakpoint = 3;
    }
  }
}
