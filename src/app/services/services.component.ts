import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  breakpoint: number;

  constructor() { }

  ngOnInit(): void {
    this.calculateBreakpoint();
  }

  @HostListener('window:resize', ['$event'])
  onResize(){
    this.calculateBreakpoint();
  }

  private calculateBreakpoint() {
    if (window.innerWidth <= 600) {
      this.breakpoint = 1;
    } else if (window.innerWidth <= 1000) {
      this.breakpoint = 2;
    } else {
      this.breakpoint = 3;
    }
  }
}
