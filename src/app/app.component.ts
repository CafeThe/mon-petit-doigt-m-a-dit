import {Component, HostListener, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {faBars} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'mon-petit-doigt-m-a-dit';

  faBars = faBars;

  sticky;
  marginContent;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private route: ActivatedRoute) {
    iconRegistry.addSvgIcon('instagram', sanitizer.bypassSecurityTrustResourceUrl('assets/img/social/instagram-icon.svg'));

    route.queryParams.subscribe(next => console.log(next));
  }

  ngOnInit(): void {
  }

  @HostListener('window:scroll', ['$event'])
  scrollHandler() {
    if (window.innerWidth > 1000) {
      this.sticky = (window.pageYOffset > 200) ? {position: 'fixed'} : null;
      this.marginContent = (window.pageYOffset > 200) ? {'margin-top': '80px'} : {'margin-top': '16px'};
    } else {
      this.marginContent = null;
    }
  }

}
