FROM node:latest AS build
COPY package.json yarn.lock /src/
WORKDIR /src
RUN yarn install
COPY . .
RUN yarn build:prod

FROM nginx:latest
COPY --from=build /src/dist/mon-petit-doigt-m-a-dit /usr/share/nginx/html
COPY dist/ .
