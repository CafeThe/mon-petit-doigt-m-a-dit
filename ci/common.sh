#!/usr/bin/env bash

SAK_IMAGE=$(grep -e "^image: " .gitlab-ci.yml | sed "s/image: //")
COMMAND="/entrypoint.sh"

if [ ! -f "/.dockerenv" ]
then
  COMMAND="docker container run --rm -it --privileged \
          -v $HOME/.docker/config.json:/root/.docker/config.json:ro \
          -v /var/run/docker.sock:/var/run/docker.sock \
          -v $PWD:/workspace:ro \
          -v $PWD/out:/workspace/out \
          $SAK_IMAGE"
fi

$COMMAND "$@"