#!/usr/bin/env bash

# exit on error
set -e

# Shellcheck tests on .sh scripts
# shellcheck disable=SC2046
ci/common.sh shellcheck -x $(find ./ci/ -type f -name '*.sh')

# Container structure tests
ci/common.sh test ci/tests/image_tests.yaml
